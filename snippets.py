# Aim of this script:
# detect MPRIS2 events (later: show in indicator)
# later, a pianobarfly script to broadcast MPRIS2
# needs:
# - playback controls
# - recording button?
# - scroll over icon == volume
# - when text is too long it scrolls (I think this is called a "marquee")

# http://amhndu.me/Articles/python-dbus-mpris.html
import dbus
import re

bus = dbus.SessionBus()

# ---- examples ---- #

# 1. retrieving list of players
# all MPRIS have prefix org.mpris.MediaPlayer2.{something}
# note: how to listen to changes in objects like this =
for service in bus.list_names():
    if re.match('org.mpris.MediaPlayer2.', service):
        print(service)

# OK. Interface "org.mpris.MediaPlayer2" has property "Identity"
# bleh but it is not a service in the bus. So what is it? players *implement* it.
# can I somehow look up '/org/mpris/MediaPlayer2'?

# 2. controlling a player
player = dbus.SessionBus().get_object('org.mpris.MediaPlayer2.clementine', '/org/mpris/MediaPlayer2')
# player.{Play,Pause,PlayPause,Previous,Next}(dbus_interface='org.mpris.MediaPlayer2.Player')

# 3. get/set properties
property_interface = dbus.Interface(player, dbus_interface='org.freedesktop.DBus.Properties')
# get
volume = property_interface.Get('org.mpris.MediaPlayer2.Player', 'Volume')
# set
property_interface.Set('org.mpris.MediaPlayer2.Player', 'Volume', volume-0.2)

# 4. song properties
metadata = player.Get('org.mpris.MediaPlayer2.Player', 'Metadata',
                      dbus_interface='org.freedesktop.DBus.Properties')
# or property_interface.Get('org.mpris.MediaPlayer2.Player', 'Metadata')
for attr, value in metadata.items():
    print(attr, '\t', value)
# xesam:contentCreated     2015-01-16T16:23:06
# xesam:album      Radioactive
# xesam:title      Radioactive
# mpris:trackid    /org/mpris/MediaPlayer2/Track/165
# xesam:useCount   2
# mpris:artUrl     file:///tmp/clementine-art-yK1220.jpg
# xesam:url    file:///home/amish/Music/Imagine Dragons/imagine-dragons---radioactive.mp3
# mpris:length     185000000
# xesam:trackNumber    1
# xesam:autoRating     38
# xesam:artist     dbus.Array([dbus.String('Imagine Dragons')], signature=dbus.Signature('s'), variant_level=1)
# xesam:genre      dbus.Array([dbus.String('Blues')], signature=dbus.Signature('s'), variant_level=1)
# bitrate      256
# xesam:lastUsed   2015-06-09T13:27:53

# The attribute name follows the "Xesam ontology". Note that the xesam:artist and xesam:genre attributes above are dbus arrays which can used like python lists, these arrays will mostly have a singular item which can be accesed this way:

print('Artist :\t', metadata['artist'][0])
# Switchfoot
# Note: Only the xesam:trackid is guaranteed to exist in the metadata dictionary, so, accessing attributes should be done with a check whether it exists.
print('Title :\t', metadata['xesam:title'] if 'xesam:title' in metadata else 'Unknown')

# 5. detecting changes
# http://stackoverflow.com/questions/23771477/how-to-continuously-monitor-rhythmbox-for-track-change-using-python-revisited
iface = dbus.Interface (player, "org.freedesktop.DBus.Properties")
track = iface.Get("org.mpris.MediaPlayer2.Player","Metadata").get(dbus.String(u'xesam:artist'))[0] + " - "+ iface.Get("org.mpris.MediaPlayer2.Player","Metadata").get(dbus.Strin$
iface.connect_to_signal ("PropertiesChanged", playing_song_changed)

def playing_song_changed (Player,two,three):
    global iface
    global track
    global home
    track2 = iface.Get(Player,"Metadata").get(dbus.String(u'xesam:artist'))[0] + " - "+ iface.Get(Player,"Metadata").get(dbus.String(u'xesam:title'))

    if track != track2:
        track = iface.Get(Player,"Metadata").get(dbus.String(u'xesam:artist'))[0] + " - "+ iface.Get(Player,"Metadata").get(dbus.String(u'xesam:title'))


# 6. *sending* song information
# pithos/plugins/mpris.py
# From project foobnix, under directory src/foobnix/thirdparty, in source file sound_menu.py.
# also see http://theravingrick.blogspot.com.au/2011/02/easily-support-sound-menu-in-python.html
def signal_playing(self):
        """signal_playing - Tell the Sound Menu that the player has
        started playing. Implementations many need to call this function in order
        to keep the Sound Menu in synch.

        arguments:
            none

        """
        self.__playback_status = "Playing"
        d = dbus.Dictionary({"PlaybackStatus":self.__playback_status, "Metadata":self.__meta_data},
                            "sv",variant_level=1)
        self.PropertiesChanged("org.mpris.MediaPlayer2.Player",d,[])

# ------------------------------------------
#
# INDICATOR TODOS:
# * set passive/active depending on whether something's playing
# * ...all the MPRIS stuff

# dconf : com.canonical.indicator.sound.interested-media-players --> must be name of desktop file
# https://wiki.ubuntu.com/Sound#Music_player_integration
# To register in the menu, each player needs to implement the MPRIS2 spec.
# The service which controls the menu automatically detects MPRIS clients appearing on DBus and reacts accordingly.
# It is essential that the implementation populates the DesktopEntry property on the MPRIS root interface. Access to the Desktop file is mandatory to register successfully.

# Hmm so I don't get to explicitly grab the icon and set its text
# So I need two parts;
# 1. a pianobar script for eventcommand that sends to MPRIS2
# 2. an indicator that reads from MPRIS2 (how has no-one done this? grab someon else's and fork it?)
# copy off pithos? pithos/plugins/notification_icon.py
# controls (play/pause etc)


