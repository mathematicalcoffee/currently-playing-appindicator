#!/usr/bin/python2

# TODO:
# grey out label/icon on pause/stop
# CAN'T FIX:
# the marquee does not have fixed width: a bug here https://bugs.launchpad.net/indicator-application/+bug/1310386
#  the best you can do is put it to the LHS of the panel. (but set_ordering_index is also not respected).
# x handle the player opening and closing --> detect changes in players/addition of players
#   can't be listened for automatically, only manually.
# work out how to disconnect dbus signals (can't, not supported, connect doesn't return a signal ID).
# DONE:
# x menu item with current song info, clicking on it toggles play/pause.
# x not just rhythmbox but /any/ player.
# x (ugly) scrolling text (marquee)
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
gi.require_version('Notify', '0.7')

from gi.repository import Gtk as gtk
from gi.repository import GLib
from gi.repository import AppIndicator3 as appindicator
from gi.repository import Notify as notify

import signal
import dbus
import dbus.mainloop.glib
import re
import os
try:  # py3
    from shlex import quote
except ImportError:  # py2
    from pipes import quote

class SongIndicator:
    APPINDICATOR_ID = 'SongIndicator'
    PLAYER = 'org.mpris.MediaPlayer2.Player'
    PROPERTIES_INTERFACE = 'org.freedesktop.DBus.Properties'
    DEBUG = False
    INDICATOR_LENGTH = 40 # characters
    INDICATOR_GUIDE = "w" * INDICATOR_LENGTH

#  ----- Menu/UI ----- #
    def build_menu(self):
        menu = gtk.Menu()

        # current song information.
        itm = gtk.MenuItem('(No song)')
        itm.connect('activate', self.cb_playpause)
        menu.append(itm)
        self.current_song_label = itm

        # rescan players
        itm = gtk.MenuItem('Rescan music players')
        itm.connect('activate', self.cb_rescan)
        menu.append(itm)

        # show notifications toggle
        itm = gtk.CheckMenuItem('Notifications')
        itm.set_active(self.show_notifications)
        itm.connect('toggled', self.cb_toggle_show_notifications)
        menu.append(itm)

        # show announce toggle
        itm = gtk.CheckMenuItem('Announce songs? (needs flite)')
        itm.set_active(self.announce_songs)
        itm.connect('toggled', self.cb_toggle_announce_songs)
        menu.append(itm)

        # quit
        itm = gtk.MenuItem('Quit')
        itm.connect('activate', self.cb_quit)
        menu.append(itm)

        menu.show_all()
        return menu

#  ----- Menu Callbacks ----- #
    def cb_toggle_show_notifications(self, itm):
        self.show_notifications = itm.get_active()

    def cb_toggle_announce_songs(self, itm):
        self.announce_songs = itm.get_active()

    def cb_rescan(self, itm):
        self.rescan_players()

    def cb_marquee_timeout(self):
        # on the marquee: the only access we have to the indicator label is
        #  'self.indicator.set_label' - ie we don't have manual control over
        #  the *position* of the label, so we can't do a proper marquee.
        # I think the best we can do is to pop characters from end to start,
        #  gross!
        self.marquee_text = self.marquee_text[1:] + self.marquee_text[0]
        self.indicator.set_label(self.marquee_text[:self.INDICATOR_LENGTH], self.INDICATOR_GUIDE)
        return True # keep going

    def cb_marquee_timeout_test(self):
        # @TODO - could do say 20 char for title, scrolling, and then 20 char for artist/album, scrolling?
        #  or would double-scrolling be too stupid?
        self.marquee_text = self.marquee_text[1:] + self.marquee_text[0]
        self.indicator.set_label(self.metadata['title'] + ': ' + self.marquee_text[:(self.INDICATOR_LENGTH - 20)], self.INDICATOR_GUIDE)
        return True # keep going

    def cb_quit(self, itm):
        self.current_song_label = None
        notify.uninit()
        gtk.main_quit()
        if self.marquee_timeout:
            GLib.source_remove(self.marquee_timeout)
        # @TODO disconnect dbus

    def cb_playpause(self, itm):
        # hmm. does not work if we are paused.
        p = self.default_player(playing=False)
        if p:
            p.PlayPause(dbus_interface=self.PLAYER)

#  ----- Dbus Callbacks ----- #
    # interface is org.mpris.MediaPlayer2.MediaPlayer
    def cb_mpris_properties_changed(self, interface, changed_properties, invalidated_properties):
        self.debug("Properties changed:\n" + str(changed_properties.keys()))

        # for rhythmbox anyway it sends lots of changed signals at song start (mostly volume)
        # for Nuvola g-play it just updates metadata but doesn't setnd STOP or PLAY
        #  and also I think it does not set track_pos == 0 (which is how audio-recorder knows to stop the recording)
        if not 'Metadata' in changed_properties:
            return

        # Note - for internet radio, the track ID doesn't change!
        metadata = changed_properties['Metadata']
        track_id = metadata['mpris:trackid']
        title = metadata['xesam:title'].encode('utf-8') if 'xesam:title' in metadata else 'Unknown artist'

        if self.old_track_id != track_id or self.old_title != title: # update label
            self.parse_metadata(metadata=metadata)

            # announce next song + prev song (do it before updating the self.old_xx)
            if self.announce_songs:
                self.announce_song()

            self.update_label()
            self.old_track_id = track_id
            self.old_title = title # note: this is not parsed.
            self.old_artist = self.metadata['artist']

            # send notification
            if self.show_notifications:
                self.notify_song()
            # @TODO store it as the most recent player (so that if we resume, it's /this/ player)

# ----- functionality ----- #
    def debug(self, msg):
        if self.DEBUG:
            print msg

    def update_label(self):
        # OLD
        self.marquee_text = '{title} - {artist} ({album})'.format(
            title=self.metadata['title'],
            artist=self.metadata['artist'],
            album=self.metadata['album']
        )
        # NEW
        #self.marquee_text = '{artist} ({album})'.format(
        #    artist=self.metadata['artist'],
        #    album=self.metadata['album']
        #)
        #self.marquee_text = self.metadata['title']
        # END
        if self.marquee_timeout: # reset the marquee
            GLib.source_remove(self.marquee_timeout)
            self.marquee_timeout = 0

        if len(self.marquee_text) > self.INDICATOR_LENGTH:
            self.marquee_text = self.marquee_text + ' | '
            if self.marquee_timeout:
                GLib.source_remove(self.marquee_timeout)
            self.marquee_timeout = GLib.timeout_add(400, self.cb_marquee_timeout)

        self.indicator.set_label(self.marquee_text[:self.INDICATOR_LENGTH], self.INDICATOR_GUIDE)

        self.current_song_label.set_label(self.metadata['title'] + '\n ' + self.metadata['artist'] + '\n ' + self.metadata['album'])

    def notify_song(self):
        notify.Notification.new(
                self.metadata['title'],
                '{artist} ({album})'.format(artist=self.metadata['artist'], album=self.metadata['album']),
                None).show() # 3rd is icon

    def announce_song(self):
        # another option: spd-say -t female3 -i 100 -r -20
        # flite -voice slt --setf int_f0_target_stddev=50 --setf int_f0_target_mean=180 --setf duration_stretch=1.2 -t
        os.system('flite -voice slt --setf int_f0_target_stddev=50 --setf int_f0_target_mean=180 --setf duration_stretch=1.2 -t ' +
                  quote('Just played %s by %s. ' % (re.sub(r'\b[Ff]eat(\.|\b)', 'featuring', self.old_title), self.old_artist)) +
                  quote('Now playing %s by %s.' % (re.sub(r'\b[Ff]eat(\.|\b)', 'featuring', self.metadata['title']), self.metadata['artist'])))
        #os.system('flite -voice slt -t ' + quote('Now playing %s by %s.' % (re.sub(r'\b[Ff]eat(\.|\b)', 'featuring', self.metadata['title']), self.metadata['artist'])))

    def parse_metadata(self, player=None, metadata=None):
        if not metadata:
            metadata = player.Get(self.PLAYER, "Metadata", dbus_interface=self.PROPERTIES_INTERFACE)

        # playerName = player.Get('org.mpris.MediaPlayer2', 'Identity',dbus_interface=self.PROPERTIES_INTERFACE)
        title = metadata['xesam:title'] if 'xesam:title' in metadata else None
        artist = metadata['xesam:artist'] if 'xesam:artist' in metadata else None
        album = metadata['xesam:album'] if 'xesam:album' in metadata else None
        # SPECIAL CASE for internet radio (streaming soundtracks specifically)
        #  which does Artist - Album - Title (duration)
        if not artist and not album and 'xesam:url' in metadata and metadata['xesam:url'].startswith('http://localhost:'):
            m = re.match('^(.+?) - (.+?) - (.+?)(?: \([0-9:]+\))?$', title)
            if m:
                artist = m.group(1)
                album = m.group(2)
                title = m.group(3)
        if artist and not isinstance(artist, basestring):
            artist = ','.join(artist)

        self.metadata = {
            'title': title.encode('utf-8') or 'Unknown title',
            'artist': artist.encode('utf-8') or 'Unknown artist',
            'album': album.encode('utf-8') or 'Unknown album',
            'track_id': metadata['mpris:trackid'] # guaranteed. OR is meant to be.
        }

    def update_players(self):
        # only adds *new* players to our list.
        # @TODO must also dump old/invalid (like when you close a player).
        bus = dbus.SessionBus()
        players = [x for x in bus.list_names() if x.startswith('org.mpris.MediaPlayer2')]
        existing_players = [p.requested_bus_name for p in self.players]
        for p in players: # p is a dbus.string
            if not p in existing_players:
                self.debug("adding new player " + str(p))
                # p is now a ProxyObject
                p = bus.get_object(p, '/org/mpris/MediaPlayer2')
                self.players.append(p)
                # @TODO send the player as signal data (that's how audio-recorder does it).
                #  Then possibly don't even need to store the player.
                p.connect_to_signal('PropertiesChanged', self.cb_mpris_properties_changed, dbus_interface=self.PROPERTIES_INTERFACE)

    def rescan_players(self):
        self.update_players()
        p = self.default_player(playing=True)
        if p:
            self.parse_metadata(p)
            self.update_label()

    # return the first playing player.
    # if playing=False and no playing player is found, it returns the first
    #  paused player, or the first player (in that order).
    def default_player(self, playing=True):
        # self.update_players() # @TODO
        if not len(self.players):
            return None

        for p in self.players:
            if p.Get(self.PLAYER, 'PlaybackStatus', dbus_interface=self.PROPERTIES_INTERFACE) == 'Playing':
                return p
        for p in self.players:
            if p.Get(self.PLAYER, 'PlaybackStatus', dbus_interface=self.PROPERTIES_INTERFACE) == 'Paused':
                return p
        return self.players[0]

#  ----- Main ----- #
    def __init__(self):
        self.show_notifications = False # music play already does it
        self.announce_songs = True
        self.old_track_id = None
        self.old_title = 'None'
        self.old_artist = 'None'
        self.indicator = None
        self.players = []

        self.marquee_timeout = 0
        self.marquee_text = ''

        self.current_song_label = None

        signal.signal(signal.SIGINT, signal.SIG_DFL) # listen to Ctrl+C
        notify.init(self.APPINDICATOR_ID)

        # if want svg, use import os first.
        # indicator = appindicator.Indicator.new(APPINDICATOR_ID, os.path.abspath('sample_icon.svg'), appindicator.IndicatorCategory.SYSTEM_SERVICES)
        # unity-icon-theme/places/svg/group-music is kind of nice.
        indicator = appindicator.Indicator.new(self.APPINDICATOR_ID, 'playlist-symbolic', appindicator.IndicatorCategory.SYSTEM_SERVICES)
        indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
        indicator.set_menu(self.build_menu())
        self.indicator = indicator

        self.rescan_players()


if __name__ == "__main__":
    dbus.mainloop.glib.DBusGMainLoop (set_as_default = True)
    ai = SongIndicator()
    gtk.main()
