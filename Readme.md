# Currently-playing appindicator

Small script that makes an appindicator for the notification tray with the title, artist, album of the current song.
Pretty crude and tied to rhythmbox atm (well, it's MPRIS, but I don't know how to listen to an arbitrary player).
No scrolling text or anything. Coudl take up a lot of space.

At the moment working (Ubuntu 16.04), but start rhythmbox first before this.
No fancy features are implemented.


Currently: have a look at http://theravingrick.blogspot.com.au/2011/02/easily-support-sound-menu-in-python.html
(sound_menu.py) for the pianobar component. Or pithos

Ok, in terms of pianobar emitting some sort of MPRIS to let appindicator know the song has changed, I see two options:

* implement MPRIS2 interface as some sort of wrapper around pianobar. The problem is EVENTCMD would be used to know when events are caught, but it is run separately each time. So I need to run my wrapper script and somehow export this instance externally (e.g. on dbus), and the EVENTCMD can simply call tell the wrapper to emit a song-changed. Rather than the wrapper script itself checking a change of song. It's not quite right. I thought I could instead (say) fake rhythmbox changing a track, but I cna't seem to do that (Metadata is write-only).
* use EVENTCMD to update a file with the current song, and appindicator can just detect file changes (rather than MPRIS) :/ easiest but pretty hacky.
